/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {PrefetchAPIReuqest, NavigatorSDK} from '@praktice/navigator-react-native-sdk-development'

const App: () => React$Node = () => {
  const meraListner = (data) => console.log(data)
  const [display, setDisplay] = useState(false)
  return (
    <View style={{ backgroundColor: 'blue', flex: 1 }}>
      {display && (
        <NavigatorSDK
          clientId="B9695830-C5A3-11E9-9187-8C85900A8328"
          searchDoctorlistner={meraListner}
          permitEmptySelection={false}
          patientGender="male"
          patientAge={42}
        />
      )}
      {!display && (
        <>
          <Text>Hey Man</Text>
          <Button onPress={() => setDisplay(true)} title="Change" />
          <>
          </>
          <Button onPress={() => PrefetchAPIReuqest({ clientId: "B9695830-C5A3-11E9-9187-8C85900A8328" }).then(console.log)} title="PRE_FETCH" />
        </>
      )}
  </View>
  );
};

export default App;
